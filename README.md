# hystrix_influxb
Exports hystrix/stream metrics in InfluxDB database

You can create an YAML configuration file like this:

```yaml
influxdb:
  server: @IP_or_server_DNS_name								(required)
  port:	8086 or whatever said in InfluxDB HTTP provider		(required)
  user:	username for this database							(optional)
  pwd:	optional if no username								(optional)
  base:	database name										(required)

clusters:
- name: example
  url: [ 'http://url.to.example/hystrix.stream' ]			(required)
  security:	http://url.to.token/genextrator/or/provider.	(optional)
  tokenIn:	Security token identifier.						(optional)
  tokenHeader: Header identifier of security token			(optional)
  headerFormat: Printf-like format for tokenHeader.			(optional)

- name: another_cluster
  url: [ 'http://url.to.another.cluster/instance1/hystrix.stream',
         'http://url.to.another.cluster/instance2/hystrix.stream' ]

- name: other hystrix
  url: [ ..., ..., ... ]
```

- security is URL of a security token provider. Response NUST be JSON stream.
- tokenIn is JSON identifier of security token in GET security response stream.
- tokenHeader will be the name used for security token in futher call to Hystrix strem URL.
- headerFormat permits small edition of security token before use in GET url header.


Run with ./hystrix_influxdb -c config.yml.

Run with -debug flag shows many debugging informations.

Metrics are send in six measurements series:

	- concurrentExecution: ???
	- isOpen:			Switch status
	- latencyExecute:	Latency execute percentiles.
	- latencyTotal:		Latency total percentiles.
	- rainbow:			All errors counters
	- requestCount: 	Request counter

Each measurements are tagged with those informations:

	- cluster:	cluster name provided in configuration file
	- breaker:	real name of hystrix switch as provided in stream
	- group:	group name provided in hystrix stream

One "cluster" may group many "breakers".

Thanks to github.com/ContaAzul/hystrix_exporter
