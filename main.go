package main

import (
	"os"
	"io"
	"fmt"
	"flag"
	"time"
	"bufio"
	"strings"
	"net/http"
	"html/template"
	"encoding/json"

	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"

	"gitlab.com/ecoc/hystrix_influxdb/config"
	"gitlab.com/ecoc/hystrix_influxdb/hystrix"
)

var Version string
var Build string

var debug = flag.Bool( "debug", false, "Show debug log" )
var configFile = flag.String( "config", "config.yml", "config file" )
var listenPort = flag.String( "port", ":9444", "listen port" )
var version = flag.Bool( "version", false, "Prints current version." )

var indexTmpl = `
<html>
<head><title>Hystrix to InfluxDB agent</title></head>
<body>
	<h1>Hystrix to InfluDB</h1>
	<h3>Configuration</h3>
	<ul>
	 {{if .InfluxCnx.Server}} <li>Serveur: {{.InfluxCnx.Server}}:{{.InfluxCnx.Port}} </li> {{end}}
	 {{if .InfluxCnx.User}} <li>User: {{.InfluxCnx.User}} </li> {{end}}
	 {{if .InfluxCnx.Base}} <li>Base: {{.InfluxCnx.Base}} </li> {{end}}
	 {{if .InfluxCnx.Retention}} <li>Retention: {{.InfluxCnx.Retention}} </li> {{end}}
	</ul>
	<h3>Clusters being monitored:</h3>
	<ul>
	{{ range .Clusters }}
		<li><a href="{{ .URL }}">{{  .Name }}</a></li>
	{{ end }}
	<ul>
</body>
</html>
`

var unflux *BaseInfos

func init() {
	log.SetHandler(cli.Default)
}

//	readHystrixSream is used to parse hystrix stream defined by Cluster URL field.
//
// - If an access denied message is seen, generate new security token and retry.
// - On other error, retry after 10 seconds wait.
// - If a line begining with "data:" is seen, report to InfluxDB.
//
func readHystrixStream( cluster config.Cluster, instance int ) {
	instance_str := fmt.Sprintf( "%d", instance )

	var ctxLog = log.WithFields(log.Fields{
										"Cluster": cluster.Name,
										"Instance": instance_str,
								})

	ctxLog.Info("reading")

	var currentToken string = ""

	if cluster.URL_Security != "" {
		currentToken, _ = getSecurityToken(cluster)
	}
	req, err := http.NewRequest( "GET", cluster.URL[instance], nil )
	if err != nil {
		ctxLog.WithError(err).Fatal("Newrequest")
	}
	if currentToken != "" {
		if cluster.HeaderFormat == "" {
			cluster.HeaderFormat = "%s"
		}
		tokenHeader := fmt.Sprintf( cluster.HeaderFormat, currentToken )
		req.Header.Set( cluster.TokenHeader, tokenHeader );
	}

	storage, err := NewStorageInfos( unflux )
	if err != nil {
		ctxLog.WithError(err).Fatal("NewStorageInfos")
	}

ReadingLoop:
	for {
		ctxLog.Debugf("Calling URL: %s", cluster.URL )
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			ctxLog.WithError(err).Error("failed to read url")
			time.Sleep(time.Second * 10)
			continue ReadingLoop
		}
		defer resp.Body.Close()

		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			line := scanner.Text()
			ctxLog.Debugf("new line: %s", line )
			if strings.HasPrefix(line, "data:") {
				report(storage, cluster.Name, instance_str, line)
			} else if strings.Contains(line, "\"Message\":\"Access Denied\"") {
				time.Sleep(time.Second * 10)
				currentToken, _ = getSecurityToken(cluster)
				continue ReadingLoop
			} else if strings.Contains(line,"error:") || strings.Contains(line,"Error:") {
				time.Sleep(time.Second * 10)
				continue ReadingLoop
			}
			continue
		}
		ctxLog.Warn("stream stop reporting")
		time.Sleep(time.Second * 10)
	}
}

//	getSecurityToken call Cluster URL_Security field and parse answer.
//
// Each JSON token in response is compared to Cluster TokenIn field.
// When a match occurs, the corresponding value is used as security token and
// send in Get header under name defined in Cluster TokenHeader field. 
func getSecurityToken( cluster config.Cluster ) (string, error) {
	var ctxLog = log.WithField( "cluster", cluster.Name )

	ctxLog.Info("security token")

	resp, err := http.Get(cluster.URL_Security)
	if err != nil {
		ctxLog.WithError(err).Warn("failed to read security token")
		return "", err
	}
	dec := json.NewDecoder(resp.Body)
	for {
		t, err := dec.Token()
		if err == io.EOF {
			break
		}
		if err != nil {
			ctxLog.Errorf("Get security token %s", err )
			break
		}
		s, ok := t.(string)
		if ok == false {
			continue
		}
		if strings.Compare( cluster.TokenIn, s ) == 0 {
			if dec.More() {
				t, err := dec.Token()
				if err != nil {
					ctxLog.WithError(err).Error("Get security token")
					break
				}
				ctxLog.Infof("currentToken: %s.", t.(string))
				return t.(string), nil
			}
		}
	}
	return "", nil
}

// report used to send parsed datas to InfluxDB.
//
func report(storage *StorageInfos, cluster, instance, line string) {
	var ctxLog = log.WithFields(log.Fields{
										"Cluster": cluster,
										"Instance": instance,
								})

	ctxLog.Info( "report" )

	data, err := hystrix.Unmarshal(line)
	if err != nil {
		log.WithError(err).Warn("failed to umarshal hystrix json")
		return
	}
	switch data.Type {
	case "HystrixThreadPool":
		fmt.Fprintf(os.Stdout, "%s %v\n", cluster, data)
	case "HystrixCommand":
//		fmt.Fprintf(os.Stdout, "%s %v\n", cluster, data)
		if unflux != nil {
			AddMeasures( unflux, storage, data, cluster, instance )
		}
	case "meta":
		return
	default:
		log.Warnf("don't know what to do with type '%s'", data.Type)
	}


	return
}
/*
 *	showMetrics to be written.
 */
func showMetrics( w http.ResponseWriter, r *http.Request ) {
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf( os.Stderr, "usage: hystrix-influxdb [option]\n" );
		flag.PrintDefaults()
		os.Exit(2)
	}
	flag.Parse()

	if *version {
		fmt.Fprintf( os.Stdout, "Version: %s\n", Version )
		fmt.Fprintf( os.Stdout, "Build Time: %s\n", Build )
		os.Exit( 0 )
	}

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.ErrorLevel)
	}

	ctxLog := log.WithFields( log.Fields{
									"appli": "hystrix_influxdb",
									"config": *configFile,
								})

	conf, err := config.Parse(*configFile)
	if err != nil {
		ctxLog.WithError(err).Fatalf("failed to parse config file: %s", *configFile)
	}
	ctxLog.Infof( "Configuration:\n%#v\n", conf )

	// Open connection to InfluxDB
	unflux = ConnectToInflux( conf )
	ctxLog.Infof( "Connected to InfluxDB:\n%#v\n", unflux )

	// Start hystrix stream readers
	for _, cluster := range conf.Clusters {
		for indice, _ := range cluster.URL {
			ctxLog.Infof( "Traitement du stream: %s instance: %d\n", cluster.Name, indice )
			go readHystrixStream( cluster, indice )
		}
	}
	// Start web server
	var mux = http.NewServeMux()
	var index = template.Must(template.New("index").Parse(indexTmpl))
	mux.HandleFunc( "/",
					func( w http.ResponseWriter, r *http.Request) {
						if err := index.Execute(w, &conf); err != nil {
							http.Error(w, err.Error(), http.StatusInternalServerError)
						}
					})
	ctxLog.Info( "Starting http server" );
	if err := http.ListenAndServe(*listenPort, mux); err != nil {
		ctxLog.WithError(err).Fatal("failed to start server")
	}
}
