package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Storage is InfluxDB server connect info
type Storage struct {
	Server		string	`yaml:"server,omitempty"`
	Port		string	`yaml:"port,omitempty"`
	User		string	`yaml:"user,omitempty"`
	Password	string	`yaml:"pwd,omitempty"`
	Base		string	`yaml:"base,omitempty"`
	Retention	string	`yaml:"retention,omitempty"`
}

// Cluster is a hystrix endpoint
type Cluster struct {
	Name			string	`yaml:"name,omitempty"`
	URL				[]string	`yaml:"url,omitempty"`
	URL_Security	string	`yaml:"security,omitempty"`
	TokenIn			string	`yaml:"tokenIn,omitempty"`
	TokenHeader		string	`yaml:"tokenHeader,omitempty"`
	HeaderFormat	string	`yaml:"headerFormat"`
}
// Config holds all clusters
type Config struct {
	Clusters []Cluster	`yaml:"clusters,omitempty"`
	InfluxCnx Storage	`yaml:"influxdb,omitempty"`
}

// Parse a file into a config instance
func Parse(f string) (config Config, err error) {
	bts, err := ioutil.ReadFile(f)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(bts, &config)

	return
}
