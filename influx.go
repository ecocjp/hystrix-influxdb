package main

import (
	"time"

	"github.com/apex/log"

	"github.com/influxdata/influxdb/client/v2"

	"gitlab.com/ecoc/hystrix_influxdb/config"
	"gitlab.com/ecoc/hystrix_influxdb/hystrix"
)

const (
	TEMPO		 = 20		// Nombre de secondes maximum entres deux envois.
	MAX_MEASURE	 = 120		// Nombre de mesures maximum stockées avant envois (120=>10sec).
	RAINBOW_MEAS = 1		// Indicateur modification des métriques du rainbow.
	IS_OPEN_MEAS = 2		// Indicateur de modification de la métrique isOpen.
	REQUEST_MEAS = 4		// Indicateur de modification de la métrique nombre de requètes.
	LAT_TOT_MEAS = 8		// Indicateur de modification de la métrique percentile total latency.
	LAT_EXE_MEAS = 16		// Indicateur de modification de la métrique percentile execute latency.
	CON_EXE_MEAS = 32		// Indicateur de modification de la métrique concurrent execution.
)

type BaseInfos struct {
	// client	InfluxDB connection handler
	client	client.Client
	// batchPointsConf configuration data needed to create 
	// a BatchPoints instance. At least Database name and Precision.
	batchPointsConf	client.BatchPointsConfig
}

type LastValue struct {
	// nextSendMap time of of next write
	nextSendMap			time.Time
	// lastIsOpen	value of last isOpen data
	lastIsOpen			bool
}

type StorageInfos struct {
	// batchPointsTable give access to BatchPoints struct for each cluster
	batchPointsTable	client.BatchPoints
	// batchPointsNumber store BatchPoints number for each cluster
	batchPointsNumber	int
	// lastValues store time of next write and last value of isOpen
	lastValues			map[string]*LastValue
}

// getNewBatch creates a new batch points table for influxDB.
func (tf *BaseInfos)getNewBatch() ( client.BatchPoints, error ) {
	return client.NewBatchPoints( tf.batchPointsConf )

}
// QueryDB will send query to InfluxDB database.
func (tf *BaseInfos)QueryDB( q client.Query ) (*client.Response, error) {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	response, err := tf.client.Query( q )

	if err != nil {
		ctxLog.WithError(err).Error( "QueryDB error" )
	} else if response.Error() != nil {
		ctxLog.WithError(response.Error()).Error( "QueryDB response error" )
	} else {
		ctxLog.Debugf( "QueryDB response: %v.\n", response.Results )
	}

	if err != nil {
		return nil, err
	} else if response.Error() != nil {
		return nil, response.Error()
	} else {
		return response, nil
	}
}

// CreateDB is used to create a new database.
// If database already exists, no error will return.
func (tf *BaseInfos)CreateDB() error {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	q := client.NewQuery( "CREATE DATABASE " + tf.batchPointsConf.Database, "", "" )
	response, err := tf.client.Query( q )
	if err != nil {
		return err
	}
	ctxLog.Debugf( "CREATE DATABASE %s: %s.\n", tf.batchPointsConf.Database, response.Results )

	return nil
}

// CreateRetentionPolicy creates an InfluDB database retention policy 
// These becomes new Stockage default retention.
func (tf *BaseInfos)CreateRetentionPolicy( ret_name string, duration string, defaut bool ) error {
	var ctxLog = log.WithField("base", tf.batchPointsConf.Database)

	ctxLog.Debugf( "SetRetentionPolicy(%s,%s,%v)\n", ret_name, duration, defaut )

	cmd := "DURATION " + duration + "REPLICATION 1"
	if defaut {
		cmd += " DEFAULT"
	}

	query := "CREATE RETENTION POLICY \"" + ret_name + "\" ON \"" + tf.batchPointsConf.Database + "\" " + cmd
	q := client.NewQuery( query, "", "" )
	response, err := tf.client.Query( q )
	if err != nil || response.Error() != nil {
		if err != nil {
			return err
		} else {
			return response.Error()
		}
	}

	tf.batchPointsConf.RetentionPolicy = ret_name

	return nil
}

// Flush send batch points to InfluxDB.
func (tf *BaseInfos)Flush( st *StorageInfos ) error {
	err := tf.client.Write( st.batchPointsTable )
	if err != nil {
		return err
	}
	bp, err := tf.getNewBatch()
	if err != nil {
		return err
	}
	st.batchPointsNumber = 0;
	st.batchPointsTable = bp

	return nil
}
// NewStorageInfos creates a new batchpoints for each cluster because
// threading requirements, the nextSendMap for intervall batching and
// set lastIsOpen to false.
//
func NewStorageInfos( base *BaseInfos ) (*StorageInfos, error) {
	var ctxLog = log.WithField("base", base.batchPointsConf.Database)
	var err error

	tf := new(StorageInfos)
	tf.batchPointsTable, err = client.NewBatchPoints( base.batchPointsConf )
	if err != nil {
		ctxLog.WithError(err).Error( "NewBatchPoints" )
		return nil,err
	}
	tf.batchPointsNumber = 0
	tf.lastValues = make(map[string]*LastValue)

	return tf,nil
}

// ConnectToInflux creates connexion context with InfluxDB server.
func ConnectToInflux( conf config.Config ) *BaseInfos {
	var ctxLog = log.WithField("server", conf.InfluxCnx.Server).WithField("base", conf.InfluxCnx.Base)
	var err error

	stock := new(BaseInfos)
	addresse := "http://" + conf.InfluxCnx.Server + ":" + conf.InfluxCnx.Port
	if conf.InfluxCnx.User != "" {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
					Username: conf.InfluxCnx.User,
					Password: conf.InfluxCnx.Password,
		})
	} else {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
		})
	}
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to connect InfluxDB")
	}

	defer stock.client.Close()

	stock.batchPointsConf = client.BatchPointsConfig{
					Database: conf.InfluxCnx.Base,
					Precision: "ns",
	}

	err = stock.CreateDB()
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to create database")
	}

	if conf.InfluxCnx.Retention != "" {
		stock.batchPointsConf.RetentionPolicy = conf.InfluxCnx.Retention
	}

	return stock
}

// AddMeasures add measurement into batchPointsTable and send datas on regular interval.
func AddMeasures( tf *BaseInfos, st *StorageInfos, data hystrix.Data, cluster string, instance string ) error {
	var ctxLog = log.WithField("cluster", cluster).WithField("base", tf.batchPointsConf.Database)

	// Fill fields and tags
	tags := map[string]string {
		"cluster":	cluster,
		"instance": instance,
		"breaker":	data.Name,
		"group":	data.Group,
	}

	timeExhausted := true
	if _, ok := st.lastValues[data.Name]; ok {
		timeExhausted = time.Now().After(st.lastValues[data.Name].nextSendMap)
	} else {
		a := &LastValue{ time.Unix(0,0), false }
		st.lastValues[data.Name] = a
	}
	stamp := time.Now()

	ctxLog.Infof( "Is timeExhausted for %s: %v now: %v nextSendMap: %v",
					data.Name,
					timeExhausted,
					stamp,
					st.lastValues[data.Name].nextSendMap )

	pleaseSend := false
	if st.lastValues[data.Name].lastIsOpen != data.Open {
		pleaseSend = true
		st.lastValues[data.Name].lastIsOpen = data.Open
	} else if timeExhausted {
		pleaseSend = true
	}
	if pleaseSend {
		// isOpen
		isOpen:=1
		if ! data.Open {
			isOpen = 0
		}
		fields := map[string]interface{}{"value":isOpen}
		pt, err := client.NewPoint( "isOpen", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create isOpen point")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Rainbow
		fields = map[string]interface{}{
			"errorCount":					data.ErrorCount,
			"errorPercentage":				data.ErrorPercentage,
			"rollingCountFailure":			data.RollingCountFailure,
			"rollingCountShortCircuited":	data.RollingCountShortCircuited,
			"rollingCountSuccess":			data.RollingCountSuccess,
			"rollingCountTimeout":			data.RollingCountTimeout,
			"rollingCountBadRequests":		data.RollingCountBadRequests,
			"rollingCountFallbackSuccess":	data.RollingCountFallbackSuccess,
		}

		pt, err = client.NewPoint( "rainbow", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create rainbow point")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Request count
		fields = map[string]interface{}{"value":data.RequestCount}
		pt, err = client.NewPoint( "requestCount", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create requestCount point")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Latency total percentile
		fields = map[string]interface{}{
			"L0":data.LatencyTotal.L0,
			"L25":data.LatencyTotal.L25,
			"L50":data.LatencyTotal.L50,
			"L75":data.LatencyTotal.L75,
			"L90":data.LatencyTotal.L90,
			"L95":data.LatencyTotal.L95,
			"L99":data.LatencyTotal.L99,
			"L995":data.LatencyTotal.L995,
			"L100":data.LatencyTotal.L100,
		}
		pt, err = client.NewPoint( "latencyTotal", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create latencyTotal points")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Latency execute percentile
		fields = map[string]interface{}{
			"L0":data.LatencyExecute.L0,
			"L25":data.LatencyExecute.L25,
			"L50":data.LatencyExecute.L50,
			"L75":data.LatencyExecute.L75,
			"L90":data.LatencyExecute.L90,
			"L95":data.LatencyExecute.L95,
			"L99":data.LatencyExecute.L99,
			"L995":data.LatencyExecute.L995,
			"L100":data.LatencyExecute.L100,
		}
		pt, err = client.NewPoint( "latencyExecute", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create latencyExecute points")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Concurrency execution count
		fields = map[string]interface{}{
			"currentConcurrentExecutionCount":data.CurrentConcurrentExecutionCount,
			"rollingMaxConcurrentExecutionCount":data.RollingMaxConcurrentExecutionCount,
		}
		pt, err = client.NewPoint( "concurrentExecution", tags, fields, stamp )
		if err != nil {
			ctxLog.WithError(err).Warn("failed to create concurrentExecution points")
		} else {
			st.batchPointsTable.AddPoint(pt)
			st.batchPointsNumber++;
		}
		// Send infos to InfluxDB
		ctxLog.Debugf("Sending measurement to InfluxDB at %s", stamp.String())

		err = nil
		if timeExhausted || st.batchPointsNumber >= MAX_MEASURE {
			err := tf.Flush( st )
			if err != nil {
				ctxLog.WithError(err).Error( "Flushing measurements" )
			}
		}
		if timeExhausted {
			st.lastValues[data.Name].nextSendMap = time.Now().Add(time.Second * TEMPO)
		}

		return err
	}

	return nil
}
