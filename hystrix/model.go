package hystrix

import (
	"encoding/json"
	"strings"
)

// Unmarshal the given string into a struct
func Unmarshal(line string) (data Data, err error) {
	line = strings.TrimPrefix(line, "data:")
	err = json.Unmarshal([]byte(line), &data)
	return
}

// Latencies of a circuit
type Latencies struct {
	L0   int64 `json:"0,omitempty"`
	L25  int64 `json:"25,omitempty"`
	L50  int64 `json:"50,omitempty"`
	L75  int64 `json:"75,omitempty"`
	L90  int64 `json:"90,omitempty"`
	L95  int64 `json:"95,omitempty"`
	L99  int64 `json:"99,omitempty"`
	L995 int64 `json:"99.5,omitempty"`
	L100 int64 `json:"100,omitempty"`
}

// Data Hystrix main data type
type Data struct {
	Type								string `json:"type,omitempty"`

	Name								string `json:"name,omitempty"`
	Group								string `json:"group,omitempty"`

	CurrentTime							int64	`json:"currentTime,omitempy"`
	Open								bool    `json:"isCircuitBreakerOpen,omitempty"`

	ErrorPercentage						int64 `json:"errorPercentage,omitempty"`
	ErrorCount							int64 `json:"errorCount,omitempty"`
	RequestCount						int64 `json:"requestCount,omitempty"`

	RollingCountBadRequests				int64 `json:"rollingCountBadRequests,omitempty"`
	RollingCountCollapsedRequests		int64 `json:"rollingCountCollapsedRequests,omitempty"`
	RollingCountEmit					int64 `json:"rollingCountEmit,omitempty"`
	RollingCountExceptionsThrown		int64 `json:"rollingCountExceptionsThrown,omitempty"`
	RollingCountFailure					int64 `json:"rollingCountFailure,omitempty"`
	RollingCountFallbackEmit			int64 `json:"rollingCountFallbackEmit,omitempty"`
	RollingCountFallbackFailure			int64 `json:"rollingCountFallbackFailure,omitempty"`
	RollingCountFallbackMissing			int64 `json:"rollingCountFallbackMissing,omitempty"`
	RollingCountFallbackRejection		int64 `json:"rollingCountFallbackRejection,omitempty"`
	RollingCountFallbackSuccess			int64 `json:"rollingCountFallbackSuccess,omitempty"`
	RollingCountResponsesFromCache		int64 `json:"rollingCountResponsesFromCache,omitempty"`
	RollingCountSemaphoreRejected		int64 `json:"rollingCountSemaphoreRejected,omitempty"`
	RollingCountShortCircuited			int64 `json:"rollingCountShortCircuited,omitempty"`
	RollingCountSuccess					int64 `json:"rollingCountSuccess,omitempty"`
	RollingCountThreadPoolRejected		int64 `json:"rollingCountThreadPoolRejected,omitempty"`
	RollingCountTimeout					int64 `json:"rollingCountTimeout,omitempty"`

	CurrentConcurrentExecutionCount		int64 `json:"currentConcurrentExecutionCount,omitempty"`

	RollingMaxConcurrentExecutionCount	int64 `json:"rollingMaxConcurrentExecutionCount,omitempty"`

	LatencyExecuteMean					int64   `json:"latencyExecute_mean,omitempty"`
	LatencyExecute						Latencies `json:"latencyExecute,omitempty"`
	LatencyTotalMean					int64   `json:"latencyTotal_mean,omitempty"`
	LatencyTotal						Latencies `json:"latencyTotal,omitempty"`

	PropertyValue_circuitBreakerRequestVolumeThreshold		int64 `json:"propertyValue_circuitBreakerRequestVolumeThreshold,omitempty"`
	PropertyValue_circuitBreakerSleepWindowInMilliseconds	int64	`json:"propertyValue_circuitBreakerSleepWindowInMilliseconds,omitempty"`
	PropertyValue_circuitBreakerErrorThresholdPercentage	int64	`json:"propertyValue_circuitBreakerErrorThresholdPercentage,omitempty"`
	PropertyValue_circuitBreakerForceOpen					bool	`json:"propertyValue_circuitBreakerForceOpen,omitempty"`
	PropertyValue_circuitBreakerForceClosed					bool	`json:"propertyValue_circuitBreakerForceClosed,omitempty"`
	PropertyValue_circuitBreakerEnabled						bool	`json:"propertyValue_circuitBreakerEnabled,omitempty"`
	PropertyValue_executionIsolationThreadTimeoutInMilliseconds	int64	`json:"propertyValue_executionIsolationThreadTimeoutInMilliseconds,omitempty"`
	PropertyValue_executionTimeoutInMilliseconds				int64	`json:"propertyValue_executionTimeoutInMilliseconds,omitempty"`
	PropertyValue_executionIsolationThreadInterruptOnTimeout	bool	`json:"propertyValue_executionIsolationThreadInterruptOnTimeout,omitempty"`
	PropertyValue_executionIsolationSemaphoreMaxConcurrentRequests	int64 `json:"propertyValue_executionIsolationSemaphoreMaxConcurrentRequests,omitempty"`
	PropertyValue_fallbackIsolationSemaphoreMaxConcurrentRequests	int64 `json:"propertyValue_fallbackIsolationSemaphoreMaxConcurrentRequests,omitempty"`
	PropertyValue_metricsRollingStatisticalWindowInMilliseconds		int64 `json:"propertyValue_metricsRollingStatisticalWindowInMilliseconds,omitempty"`
	PropertyValue_requestCacheEnabled						bool	`json:"propertyValue_requestCacheEnabled,omitempty"`
	PropertyValue_requestLogEnabled							bool	`json:"propertyValue_requestLogEnabled,omitempty"`

	ReportingHosts											int64	`json:"reportingHosts,omitempty"`
}
